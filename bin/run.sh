#!/usr/bin/env bash

# Default Variables
WIKI_PATH=
PORT=5000

show_help() {
cat << EOF
  Usage: ${0##*/} [-h] [-e EDITION] [-p PORT]

  Runs the Tiddlywiki at PATH

EOF
}

while getopts he:p: opt; do
	case ${opt} in
		h)
			show_help
			exit 0
			;;
		e)
			WIKI_PATH=$OPTARG
			;;
		p)
			PORT=$OPTARG
			;;			
		*)
			show_help >&2
			exit 1
			;;
	esac
done

if [[ ! -z WIKI_PATH ]] && type tiddlywiki &> /dev/null ; then
	echo "Running Wiki at ${WIKI_PATH}"
	tiddlywiki "./editions/${WIKI_PATH}" --server ${PORT}
else
	echo "Tiddlywiki not installed or no path specified"
	exit 1
fi
