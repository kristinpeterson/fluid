[![Build Status](https://gitlab.com/eschlon/fluid/badges/master/pipeline.svg)](https://gitlab.com/eschlon/fluid/commits/master)

# Fluid Wiki

Welcome to the GitHub repository for the [Fluid wiki](https://eschlon.gitlab.io/fluid/)

## Install

Fluid wiki requires [Node.js](https://nodejs.org/en/) and [tiddlywiki](http://tiddlywiki.com/#GettingStarted).

Once Node.js is installed, install tiddlywiki with the following command: 

```
$ npm install -g tiddlywiki
```

## Run

To run the Fluid wiki locally:

```
$ tiddlywiki fluid --server
```

Then, visit http://127.0.0.1:8080/ in your browser.
