/*\
title: $:/talus/macros/reorder.js
type: application/javascript
module-type: macro

Reorders a comma separated list, placing the element that includes some substring at the beginning of the list.
\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

exports.name = "reorder";

exports.params = [
	{name: "title"},
	{name: "pattern"},
	{name: "field"}
];

exports.run = function(title, pattern, field) {
	title = title || this.getVariable("currentTiddler");
	pattern = pattern || this.getVariable("currentTiddler");
	field = field || "reqs";

	var tiddler = !!title && this.wiki.getTiddler(title);
	var string = "";

	if (tiddler) {
		string = tiddler.getFieldString(field)
	}
		
	if (string) {
		return string.split(/, */).sort((e) => (!e.includes(pattern))).join(', ')
	}
};

})();
