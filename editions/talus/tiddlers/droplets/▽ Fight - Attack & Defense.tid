caption: ▽ Attack & Defense
created: 20210401061137831
modified: 20210412061049548
tags: [[▽ Fight]]
title: ▽ Fight - Attack & Defense
type: text/vnd.tiddlywiki

The sole resolution mechanic in Fluid is the [[Test|Tests]], so it likely comes as no surprise that conflicts are just a series of tests. Individuals in a conflict declare goals, which are generally aimed at gaining advantage over their opponent, roll dice and bid successes toward their aims.

! Conflict, Goals & Narrative Control
Causing harm to an opponent is just like any other test: declare a compatible goal, roll a test and bid successes against your goal. The only real difference between attacking an opponent and picking a lock is that locks don't typically fight back. While the difficulty of opening a lock is generally just some static value, an inentional opponent can present a dynamic difficulty as they struggle to achieve their own goals, while preventing you from achieving yours.

<<<.example
Jonah is facing off against an orc chieftan in a duel to the death. As the fight begins she narrates that she's moving in cautiously focusing on defending herself against the much larger opponent. The orc meanwhile charges in with a goal of taking her head off.

Jonah rolls 7 successes. The orc rolls 5 successes and bids all of them to his attack. Jonah decides to bid all 7 of hers to oppose the orcs blow. The orc is now facing a difficulty of 7, and fails his test by a margin of 2. 

Jonah takes narrative control and narrates that she deftly sidesteps the orcs clumsy strike and lands a weak slash across his back. Jonah's goal wasn't aimed at harming the orc, so takes his 2 die margin of failure as a //Consequence//.
<<<

It's worth noting at this point that Fluid doesn't really have a notion of //opposed rolls//. Two opponents may have incompatible or opposing //goals//, but they're not rolling against one another directly. Each combatant bids successes toward their own goals. If those goals happen to be incompatible then the any successes bid against them serve as a dynamic difficulty value with the greater pool succeeding and gaining narrative control as usual. It's a sublte distinction best illustrated by continuing our example.

<<<.example
After half an hour of fighting both opponents are still standing but wounded and exhausted. At this point Jonah is sure that she's far more skilled than her orcish opponent, and suspects that his strength is waning and decides it's time to end it. 

She narrates that she charges in with an aim of slaying her opponent. The agressive orc has a similar goal. There's no particular reason that these goals are incompatible, both opponents are just single mindedly trying to chop each other to bits, so both tests are unopposed at a difficulty of 0.

Jonah rolls 8 successes and bids all of them toward harming the orc. Meanwhile, the tired orc rolls 4 successes bids them similarly. Both sides succeed and have narrative control for their goals.

Jonah is able to negotiate a 4 severity //Condition// by paying the tax but is sorely wounded. The orc narrates that he buries his axe deep into her shoulder, dropping her to one knee with the weight of the blow.

The orc, however, is not able to pay the taxes on his 8 die //Condition// and has nothing to negotiate with. Jonah narrates that she bites through the pain and drives her sword up through the orc's head.
<<<

! Damage & Negotiation
As the above examples demonstrate damage in Fluid is just some form of [[Tax|Taxes]] in the form of a [[Condition|Costs, Conditions & Consequences]] or [[Consequence|Costs, Conditions & Consequences]], where the core distinction is intent. It can be a bit fuzzy at times, but generally if your goal is to directly harm your opponent then it's a //Condition//, otherwise it's a //Consequence//. Area damage and damage caused by devices like grenades and similar are also usually //Consequences//.

Regardless of whether you failed to defend yourself, were unable to do so, or just chose not to, these taxes are always considered the result of a test, and in Fluid the result of a is a //negotiation//. The individual that caused the tax has narrative control, but you have the opportunity to negotiate either by simply paying the tax to remain conscious and in play, or by using [[Talents|Talents & Masteries]] to change the rules of the game.

After this the victor gets to narrate the outcome, but has to do so within the boundaries of negotiation. If your goal was to lop your opponent's head off, but they're able to negotiate by paying the tax, then you'll need to reign in your goal in your narration. If they can't, then let the heads roll.