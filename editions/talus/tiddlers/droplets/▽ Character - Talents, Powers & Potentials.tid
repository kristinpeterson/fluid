caption: ▽ Talents, Powers & Potentials
color: #0433ff
created: 20210311062035275
modified: 20210313222537006
tags: [[▽ Character]]
title: ▽ Character - Talents, Powers & Potentials
type: text/vnd.tiddlywiki

In addition to [[Facets]] characters will also eventually accumulate a number of [[Talents|Talents & Masteries]] which enable them to bend the rules, perform great feats and manifest heroic powers. Using these abilities generally involves paying some [[Tax|Taxes]] to one or more facets and often also has some narrative requirement. The former puts relatively strict limits on the use of these powerful abilities, while the latter works to keep the focus on telling a good story.

As a character progresses into more heroic territory they may also acquire one or more [[Potentials|▽ Potential]], powerful facets representing the character's supernatural or legendary ability in some domain. These facets are expensive to improve, but provide great advantages and help differentiate the legendary hero (or evil archmage) from the common adventurer. 

In this setting [[Magic|▽ Magic]] is also handled by this system. Esoterically sensitive individuals may begin with basic abilities in some flavor or //Esoteric Potential//, typically as the result of a //Lineage//. Others may eventually discover such potential later during play. Regardless, such characters face a long and dangerous road if they wish to reach their full potential.

{{||#expand}}