created: 20210324175146745
modified: 20210330041607299
reqs: 1[[✦ Daedal Potential]], 5[[❖ Crafting]]
source: ▽ Craft
tags: [[✦ Daedal Potential]] Talent [[❖ Crafting]]
title: ♣ Alchemy
type: text/vnd.tiddlywiki

Alchemists acquire and invent //Formulae// to produce potions, salves, oils, powders and other chemicals that have magical properties. //Formulae// always require three components: a chemical base, a catalyzing reagent and a container to keep the chemical inert and preserved until use.

This talent allows the Alchemist to invent formulae, fabricate daedal items from their formulae and fully utilize alchemical equipment and materials. 

On any [[Test|Tests]] to learn or invent a new property or to fabricate a daedal item they may narrate that one or more of their required components provide additional benefits to the attempt based on their quality and rarity.

|full-width|k
|! Nature|! Bonus|! Examples|
| Mundane | $$+Qp$$ | Oil, Purified Water, Tree Sap |
| Rare | $$+Qd$$ | Deep Glacial Ice, Gold Flakes, Pickled Eye of Newt |
| Daedal | $$+Qs$$ | Powdered Dragon Bone, Unicorn Tears, Gnomish Geode |