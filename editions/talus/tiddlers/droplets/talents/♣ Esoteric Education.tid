created: 20210322011708874
modified: 20210331083111120
reqs: 1[[✦ Esoteric Potential]], 3[[❖ Scholarship]]
source: ▽ Magic
tags: Talent [[❖ Scholarship]] [[✦ Esoteric Potential]]
title: ♣ Esoteric Education
type: text/vnd.tiddlywiki

You've been trained in the basic primitive principles and techniques of the esoteric arts and are able to benefit from instruction and formal education in the esoteric arts. You may learn new //Powers// either from a teacher or from notes and instructional documents and record these powers as //Spells// in a spellbook instead of paying to acquire them as [[Talents|Talents & Masteries]].

Spellbooks are items capable of recording a number of //Ranks// of //Spells// equal to twice their //Quality//. Learning a new //Spell// requires both the act of practicing and learning the technical minutae required to manifest the ability and recording the power to one of your spellbooks.

Learning a new //Spell// requires an appropriate [[Test|Tests]] against a difficulty equal to the //Rank// of the //Spell// if you have a willing and helpful teacher, or twice the //Rank// if you are reconstructing it from notes and instructional texts. It takes 1 week per //Rank// to acquire a new //Spell// in this way; should the learning test fail you may try again by studying for the required duration again.

You may manifest any //Spell// recorded in any one of your accessible spellbooks by first paying its cost and then succeeding in a test against the //Rank// of the //Spell//. If the test fails the you suffer a //Consequence// with a severity equal to your margin of failure //plus// the cost of the //Spell//.