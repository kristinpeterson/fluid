created: 20210317024721555
modified: 20210330040521487
reqs: 3[[❖ Marksmanship]]
source: ▽ Fight
tags: [[❖ Marksmanship]] Talent
title: ♣ Crossbow Proficiency
type: text/vnd.tiddlywiki

You are trained at using crossbows properly and effectively. Crossbows come in a variety of sizes, from small wrist mounted weapons to large powerful arbalests.

Crossbows are a compact, precise ranged weapon that propels bolts using mechanical forces similar to a bow. Unlike a bow, however, crossbows are entirely mechanical. The weilder readies the crossbow either by drawing and locking it manually (for small crossbows) or by using a lever, winch or windlass. Once readied a crossbowman merely aims and pulls a trigger to fire. While the additional mechanical complexity makes crossbows slower to ready and fire than a bow, keeping them at the ready places no stress on the wielder's body making them much easier to use and aim.

You have a [[Mastery|Talents & Masteries]] in in weapons in the class granting //Advantage// in any narratively relevant tests. Your extensive training also allows you treat any weilded crossbow as [[♦ Durable]]. If the weapon already has the [[♦ Durable]] property you may pay ''2x Stamina'' instead of the usual ''3x Stamina'' when activating the property.