color: #fff995
created: 20210324170528300
modified: 20210404211557252
tags: [[Potential Talents]] Potential
title: ✦ Eldritch Potential
type: text/vnd.tiddlywiki

The universe is host to an uncountable number of eldrich entities. While some of these entities take interest in mortal affairs, these beings exist on an entirely different level of reality and view even the most powerful mortals as fleeting and largely inconsequential in the grand scheme of things. Eldrich entities may take interest in individual mortals from time to time, and may even grant powers or hear prayers from select individuals, but this by choice on their terms.

Your [[✦ Eldritch Potential]] makes you an exception, when you speak the universe listens. Your potential doesn't merely allow you to communicate with eldrich beings, anyone can offer a prayer to the sprit of the ocean or whisper an bargain to a dark god. When you engage in diplomacy with an entity it listens intently and takes negotiations and agreements seriously. Perhaps eldrich forces of the universe believe you are one of their own, perhaps they're right. Regardless of the reason, true masters are capable of learning to summon eldrich beings, court and draw power from sympathetic entities and even bind entities to their will.

These potentials are often focused along specific cultural groups (e.g. //Anu Shamanism//), specific entity types (e.g. //Elemental Diplomacy//) or specific entity qualities (e.g. //Stone Speaking//). Some rare individuals are also blessed and/or cursed with //The Voice// a powerful potential that roughly puts the individual on speaking and negotiating terms with the entirety of the eldritch world.