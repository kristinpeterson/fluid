caption: ▽ Natural Talent & Marvels
created: 20210404045423086
modified: 20210404062042611
tags: [[▽ Magic]]
title: ▽ Magic - Natural Talent & Marvels
type: text/vnd.tiddlywiki

Merely having a relevant //Potential// is powerful enough to begin to manifest your abilites. You are free to use your //Potential// as a [[Facet|Facets]] for [[Tests]] and [[Taxes]], you may also use it additively under //advantage// as described in [[▽ Potential]]. This alone gives you significant advantages over common folk and allow you to solve mundane problems in supernatural ways.

<<<.example
Malik was born with an affinity for elemental fire, and possess the esoteric potential //Gift of Flame// at rank 6. Unfortunately his gifts are not well recieved by the superstitious locals and he finds himself locked in a cage awaiting his execution for witchcraft. He's not great at picking locks, being able to put up $$3d$$ from his combined //Agility// and //Engineering// but his //Survivor// trait gives him advantage in this case allowing him to add his $$6d$$ potential to the attempt. With the help of his gift he successfully opens the lock to make his escape.

He narrates that he grasps the shackle of the lock firmly concentrating on his fear until it glows red hot, and then uses a rock to pry the softened metal apart.
<<<

For keeping things tidy these basic manifestations of your potential are called //Marvels// and doing anything significant is just like any other [[Test|Tests]]. The GM will set a general difficulty of performing the //Marvel// and you can use any dice in your margin of success to narrate the effect. Manifesting powers in this way is both taxing and unreliable, for setting difficulty select use the following guide adding 2 to the difficult for each step above the first, or use the more detailed guide in [[▽ Powers]] but skip the final step of dividing by 3.

;Duration
: Test | Turn | Turns | Scene | Session 
;Range
: Touch | Thrown | Ranged | Artillery | Remote
;Area
: Personal | Small | Medium | Large | Huge

<<<.example
On his way out of the town Malik is attacked by a zealot and decides to use his gift to defend himself, as his weapons have all been confiscated. He raises his hand and narrates that he's concentrating on projecting a jet of flame at his opponent. This is simple power with a default duration and area, but its //Thrown// range increases it's difficulty to 2. Malik is again under //advantage// and uses $$3d$$ from his //Focus// and //Throwing// skill to augment his $$3d$$ potential. He rolls 9 successes, passing by 7. He bids his extra successt to deals a 7 Severity //Condition// to his attacker runs past the stunned zealot.
<<<

Failing a test has the normal consequences, though the narration of those consequences is often supernatural in character.

<<<.example
Outside of town Malik hears the bark of dogs and panics. He knows he can't outrun them in his current state, so he turns and narrates that he touches the ground and calls forth a wall of fire to give himself a head start. This is a more significant power with a difficulty of 8 ($$+4$$ for a medium area, $$+4$$ for multiple turn duration). He gathers his dice and rolls a 6, failing his test by 2 and losing narrative control. 

The GM narrates that as he touches the ground the grass around his hand is reduced to ash, but then he feels a sudden surge of heat sear down his arm and loses his concentration, failing to manifest the //Marvel//. Malik takes a $$2d$$ //Consequence// as a result of the backfire.
<<<