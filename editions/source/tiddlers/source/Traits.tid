caption: [img height=10 [fluid.svg]] {{!!title}}
created: 20210209072539222
icon: fluid.svg
modified: 20210214225214947
tags: Primitives
title: Traits
type: text/vnd.tiddlywiki

''Traits'' provide a basic description of the essential characteristics of an [[entity|Entities]]. Traits are represented as a list of words or short phrases that define what an entity is, its properties, and/or why it behaves as it does. Traits are neither good nor bad, and can provide either [[advantage|Bonuses & Penalties]] or [[disadvantage|Bonuses & Penalties]] to the entity depending on the context.

<<<.example
Jonah is a veteran barbarian in search of her lost brother. She takes the traits ''Brave'' and ''Must find my brother''. On her quest she encounters an ancient dragon and uses her ''Brave'' trait to give her //Advantage// when testing against the dragon’s fear, handily slaying the beast. In its remains she discovers both the location of its lair and a clue to the whereabouts of her lost sibling. She decides to go for the treasure, and the GM imposes //Disadvantage// due the guilt of ignoring the plight of her brother.
<<<

//Traits// are intentionally free-form and can be removed and added to an entity as their properties or goals change. The core aim is to describe how the entity behaves. Source has no explicit limitations on the number of traits an individual character can have, though specific [[Droplets|▽ Fluid Droplets]] may impose such restrictions.