color: #0433ff
created: 20210213063242553
modified: 20210313221723621
tags: [[▽ Fluid Droplets]]
title: ▽ Aptitude & Skill
type: text/vnd.tiddlywiki

Player characters are the most important entity in most settings. How you choose to represent player characters (and other important NPCs) has significant impact on gameplay and on the kind of narrative that will result. For some narratives going with a simple, flexible representation such as that presented in [[▽ Dramatis Personae]] will work fine. In other cases something more rigid and complex is warranted. 

[[▽ Aptitude & Skill]] is the foundation for a modular system for representing these important characters. Characters are represented using two related systems of [[Facets]] with //Skills// representing the character's specific training and //Aptitudes// representing their more general ability in various domains. It also provides a number of new [[Talents|Talents & Masteries]] related to these facets to add more customization and flavor.

This droplet does add significant complexity to the character sheet, and this means it will slow down character creation and require players to do more work and bookkeeping. It's best suited settings where the player characters are durable protagonists and the increased complexity is justified by:

* Greater depth of customization and growth potential
* Increased consistency and balance

Many players also find it easier to fill out a rigid character sheet and select from a set of pre-designed skills and powers than to start with a blank canvas and come up with their own design. This system helps with that somewhat; however, this system also relies heavily on narrative requirements and is intentionally vague in ways that reward narrative creativity.

{{||#droplet}}