caption: ▽ Aspects
created: 20210215005231829
modified: 20210310064024603
tags: [[▽ Dramatis Personae]]
title: ▽ Dramatis Personae - Aspects
type: text/vnd.tiddlywiki

Characters with a significant role in the story will eventually benefit from having a small system of //Aspects// in addition to their //Traits//. //Aspects// are just [[Facets]] with a value ranging from $$1d$$ to $$10d$$ that represent the areas in which the charater is specialized and their capabilities in those domains. This adds complexity and bookkeeping but also provides a few benefits:

* A character's //Aspects// can be expanded or advanced over time, representing growth and development of the character.
* Different characters may have different //Aspects// and/or different ranks in those aspects, adding additional opportunities for customization and differentiation.

If the character is already developed one should lean on their //Traits// to build out a set of suitable starting //Aspects// and their ranks.

<<<.example
After a few weeks of searching the party has discovered that Jonah's lost brother was captured and pressed into service as a gladiator. Unfortunately, he has since been taken north to the Imperial Capital to fight in the arena. As the party prepares to depart they have a discussion with Malik and agree that it would be best to have someone who knows the culture and language if they're going to succeed. Malik joins the group as an NPC.

As a companion Malik's role in the narrative is now much more prominent so the GM decides that he deserves to be fleshed out further and assigns a few //Aspects// based on his traits and how his character has developed over the past few sessions.

; Malik the Guide
: Akin Guide, Ex-Baker's Apprentice, Orphan
: ''Akin Etiquette'': $$5d$$
: ''Streetwise'': $$7d$$
: ''Baking'': $$3d$$
<<<

|float-right|k
|! Aspect Rank|! Dice|! Cost to Obtain|! Cumulative Cost|
| Familiar |  1d | 1xp  |  1xp |
|~|  2d | 2xp  |  3xp |
| Trained  |  3d | 3xp  |  6xp |
|~|  4d | 4xp  | 10xp |
| Professional |  5d | 5xp  | 15xp |
|~|  6d | 6xp  | 21xp |
| Expert |  7d | 7xp  | 28xp |
|~|  8d | 8xp  | 36xp |
| Master |  9d | 9xp  | 45xp |
|~| 10d | 10xp | 55xp |

At this point the character should grow appropriately as the narrative progresses. If the character is under the direction of the GM then the GM can just adjust their //Aspects// or add new ones as things go forward. If the character is under player direction then the GM and the player should collaborate to grow the character and use [[Experience]] to guide the pace of that growth. //Aspects// follow triangular growth as described in [[Source|Facets]] and shown in the table to the right.

<<<.example.clear
On the journey to the Imperial Capital, Fin takes Malik under his wing and spends some time teaching him how to defend himself. The GM collaborates with Fin and they decide that over the course of their adventures he's reasonably accumlated $$4xp$$ for Fin to use to expand his capabilities. Fin spends the journey instructing Malik in his particular style of self-defense.

; Malik the Guide
: Akin Guide, Ex-Baker's Apprentice, Orphan
: ''Akin Etiquette'': $$5d$$
: ''Streetwise'': $$7d$$
: ''Baking'': $$3d$$
: ''Dirty Fighting'': $$2d$$ (costing $$3xp$$)
: ''Stealth'': $$1d$$ (costing $$1xp$$)
<<<

At this stage the character can participate in pretty much any mechanical aspect of the system including making or assisting in [[Tests]], paying [[Taxes]] and developing through [[Advancement]].