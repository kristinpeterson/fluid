color: #0433ff
created: 20210213221456382
list: [[▽ Powers - Ranks]] [[▽ Powers - Examples]]
modified: 20210313221633837
tags: [[▽ Fluid Droplets]]
title: ▽ Powers
type: text/vnd.tiddlywiki

Powers is a meta-droplet in that it doesn't really represent any particular [[Entity|Entities]]. Instead it provides some guidance around judging the relative power and effectivness of //Talents//.

One of the primary undertakings of building out a droplet is typically to construct one or more [[Talents|Talents & Masteries]] to represent special powers or abilities that the [[Entities]] covered by that droplet can tap. Creating Talent is relatively simple, after all, they can do pretty much anything. Balancing Talents against each other and figuring out appropriate costs and requirements is a bit more tricky. 

During play there isn't usually a need for any formal system to handle this. If the GM or player ends up making a new talent (e.g. as a result of [[Character]] expenditures or for some other need) it's usually sufficient to just collaborate on what the new Talent does, and come to an understanding on a narratively appropriate cost for using it.

When building a droplet or in the context of some of more generative droplets (e.g. a droplet that allows crafting or researching new magical spells) it can be useful to have a rough system to rank Talents based on thier effectiveness. This droplet provides one such system.

{{||#droplet}}