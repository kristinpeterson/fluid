caption: ▽ Examples
created: 20210214080646699
modified: 20210310064123087
tags: [[▽ Powers]]
title: ▽ Powers - Examples
type: text/vnd.tiddlywiki

Here are a few examples of talent design for a few different cases.

! Sharpness Property

The [[Gear|▽ Gear]] droplet introduces a type of talent called a //Property// for adding special effects to items and equipment. On of those properites is [[♦ Sharp]]. When designing this property we start with a basic description of what we're trying to represent.

<<<
This weapon has a particularly keen edge. It is ''more effective'' against ''unarmored or lightly armored opponents''.
<<<

We then decided to formalize ''more effective'' as $$+1s$$. Additionally we decided to incurr a tax in the item's quality both to represent the quality degredation of the weapon over time and also to make using the property an interesting decision for the player (rather than a static passive bonus they have to bookkeep and remember). This resulted in the talent as it exists:

{{♦ Sharp||#talent}}

The property is a $$1s$$ fixed bonus ($$+2$$) with a $$1x$$ fixed cost per use ($$-1$$) and a limited application to lightly armored opponents ($$-3$$). Adding that all together gives us a total of -2, and dividing by 3 and rounding we get a talent rank of -1. This is what we were going for, a basic mundane property to give some specially crafted weapons a bit more flavor.

! Classic Fireball
If we were to build a talent to represent the classic fireball it'd look something like this:

<<<
You collect a baseball sized ball of fire into your hands and ''project it in a straight line at a target''. The projectile explodes either when it reaches its target or if it impacts any physical intervening object causing ''a variable amount of damage'' that is ''dependent on how powerful you are'' to any entity In ''a pretty large area''.
<<<

Let's start translating that into mechanics and rank. Since damage is both variable and dependent on the character's ability we decide to represent it as a a //Scaled Reliable Bonus//, basically it'll do $$Xd$$ damage where $$X$$ is some facet that the wizard can improve. We'll just call that facet //Potential// and say that it has a max value of $$10d$$. The classic area of effect is a 20' radius. The range is line of sight, so it could be thrown or ranged, but since intervening objects can get in the way we'll just classify it as thrown.

{{★ Fireball||#talent}}

Putting that all together we have an scaled unreliable of $$\max(\text{Arcane})d$$ where //Arcane// goes up to $$10d$$ ($$+5$$), a thrown range ($$+2$$), a group area ($$+4$$) and no costs or requirements to speak of. That gives us a total of 11 and a rank of 3.

! Aptitude Master Talents
The [[▽ Aptitude & Skill]] droplet defines a set of character [[Facets]] called //Aptitudes// that represent the character's abiltes in different domains. It also provides a set of new talents that use these facets as requirements. Here we'll just focus on the top-level master talents for each aptitude. 

Each aptitude has only one of these talents and they always have both a high requirement (e.g. the associated facet must be at max rank) and a high cost ($$3x$$ tax). They are supposed to be very powerful rewards for reaching peak ability in their respective domain, but we also want them to all be roughly //equally// powerful. Here we'll just consider the three presented below.

{{♣ Omniscience||#talent}}
{{♣ Potency||#talent}}
{{♣ Master Plan||#talent}}

Powerful indeed. Let's see what rank they are.

[[♣ Omniscience]] is purely mechanical and so pretty straight forward. It grants a $$2t$$ bonus for the scene with a $$3x$$ tax but no other costs or restrictions. That's $$+9$$ for the bonus, $$+4$$ for the duration and $$-3$$ for the tax. That gives us a total of 10 for a final rounded rank of 3.

[[♣ Potency]] is also mechancial but a bit more tricky. It's an automatic success but also a scaled reliable margin on the character's current //Vigor// which has a max of 5. There's no duration to speak of but it does have a contextual usage restriction to //feats of strength//. Putting that together we'll take $$+10$$ for the success and $$+5$$ for the scaled margin and adjust by $$-3$$ for the tax and $$-3$$ for the usage. That gives us a total of 9 for a rank of 3.

[[♣ Master Plan]] seems pretty impossible to rank, but we can do it. Basically it lets the player both succeed and permanently alter the narrative (and perhaps re-interpret history) but only when they've first failed. In addition to the tax, there is also a pretty extensive narrative requirement. So we have $$+10$$ for the success and another $$+10$$ for the permanence, then we adjust by $$-3$$ for the tax, $$-3$$ for the failure requirement and $$-5$$ for the narrative cost of having to deliver a believable retcon. That gives us a total of 9 for a rank of 3.